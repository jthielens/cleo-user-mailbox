package com.cleo.harmony.sample.mailbox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.cleo.lexicom.external.ILexiCom;
import com.cleo.lexicom.external.LexiComFactory;

public class CreateUserMailbox {

    /*------------------------------------------------------------------------*
     * Output to stdout and stderr                                            *
     *------------------------------------------------------------------------*/

    /**
     * Prints a status message on stdout.
     * @param message the message
     */
    private static void report(String message) {
        System.out.println(message);
    }

    /**
     * Prints an error message on stderr.
     * @param message the message
     */
    private static void error(String message) {
        System.err.println(message);
    }

    /*------------------------------------------------------------------------*
     * Very basic command line parser                                         *
     *------------------------------------------------------------------------*/

    /**
     * Prints an optional error message and usage instructions on stdout.
     * @param message a (possibly null) error message
     */
    private static void printUsage(String message) {
        if (message!=null) {
            report(message);
        }
        report("usage: CreateUserMailbox [options] user ...\n"+
               "options: -d  directory  Harmony home directory (defaults to .)\n"+
               "         -h  name         local user host to use\n"+
               "         -p  protocol     protocol: ftp (default), sftp, or http\n"+
               "         -t  name         template mailbox to use (defaults to myTradingPartner)");
    }

    /**
     * The Harmony home directory.
     */
    private static File homeOption = new File(".");

    /**
     * The local user host to use -- null means go find one.
     */
    private static String hostOption = null;

    /**
     * The local user protocol to use.
     */
    private static int protocolOption = ILexiCom.FTP_SERVER;

    /**
     * The template account to use.
     */
    private static String templateOption = "myTradingPartner";

    /**
     * Parses the command line and returns the non-option elements as the
     * list of users.
     * @param args the command line arguments
     * @return the non-option arguments, or null if an error occurred.
     */
    private static String[] parseArgs(String[] args) {
        // usage: CreateUserMailbox [options] user ...
        // options: -d  directory    Harmony home directory (defaults to .)
        //          -h  name         local user host to use
        //          -p  protocol     protocol: ftp (default), sftp, or http
        //          -t  name         template mailbox to use (defaults to myTradingPartner)
        List<String> users = new ArrayList<String>(args.length);
        for (int i=0; i<args.length; i++) {
            String arg = args[i];
            if (arg.startsWith("-")) {
                if (arg.equals("-d")) {
                    i++;
                    if (i < args.length) {
                        homeOption = new File(args[i]);
                        if (!homeOption.exists()) {
                            printUsage("not found: "+args[i]);
                            return null;
                        } else if (!homeOption.isDirectory()) {
                            printUsage("directory expected: "+args[i]);
                            return null;
                        }
                    } else {
                        printUsage("directory name required for -d");
                        return null;
                    }
                } else if (arg.equals("-h")) {
                    i++;
                    if (i < args.length) {
                        hostOption = args[i];
                    } else {
                        printUsage("host name required for -h");
                        return null;
                    }
                } else if (arg.equals("-p")) {
                    i++;
                    if (i < args.length) {
                        if (args[i].equalsIgnoreCase("ftp")) {
                            protocolOption = ILexiCom.FTP_SERVER;
                        } else if (args[i].equalsIgnoreCase("sftp")) {
                            protocolOption = ILexiCom.SSHFTP_SERVER;
                        } else if (args[i].equalsIgnoreCase("http")) {
                            protocolOption = ILexiCom.HTTP_SERVER;
                        } else {
                            printUsage("protocol must be ftp, sftp, or http: "+args[i]);
                            return null;
                        }
                    } else {
                        printUsage("protocol required for -p");
                        return null;
                    }
                } else if (arg.equals("-t")) {
                    i++;
                    if (i < args.length) {
                        templateOption = args[i];
                    } else {
                        printUsage("template name required for -t");
                        return null;
                    }
                } else {
                    printUsage("unregonized option: "+arg);
                    return null;
                }
            } else {
                users.add(arg);
            }
        }
        return users.toArray(new String[users.size()]);
    }

    /*------------------------------------------------------------------------*
     * Harmony API                                                            *
     *------------------------------------------------------------------------*/

    /**
     * The handle to the API.
     */
    private static ILexiCom lexicom;

    /**
     * Finds a local user host with the indicated protocol
     * @param protocol the protocol
     * @return the name of a local host supporting {@code protocol}, or {@code null}
     */
    private static String findHost(int protocol) {
        try {
            for (String host : lexicom.list(ILexiCom.HOST, new String[0])) {
                if (lexicom.isLocal(host) && lexicom.getHostProtocol(host)==protocol) {
                    report("found local user host "+host);
                    return host;
                }
            }
        } catch (Exception ignore) {}
        return null;
    }

    /**
     * Returns the host to be used to create users for the indicated protocol,
     * either from the command line option or by searching the list of
     * active hosts.  Returns the host name to use, or null in case of error.
     * @param protocol the protocol
     * @return the name of the host to use, or {@code null}
     */
    private static String resolveHost(int protocol) {
        try {
            if (hostOption!=null) {
                if (exists(hostOption)) {
                    if (lexicom.getHostProtocol(hostOption)==protocol) {
                        report("adding users to host "+hostOption);
                        return hostOption;
                    } else {
                        error("host "+hostOption+" is not the correct protocol");
                    }
                } else {
                    error("host "+hostOption+" does not exist");
                }
            } else {
                String host = findHost(protocol);
                if (host!=null) {
                    return host;
                }
                error("can't find local user host");
            }
        } catch (Exception ignore) {}
        return null;
    }

    /**
     * Checks if a host exists.
     * @param host the host
     * @return true if the host exists
     */
    private static boolean exists(String host) {
        try {
            return lexicom.hasProperty(ILexiCom.HOST, new String[] {host}, "alias");
        } catch (Exception ignore) {}
        return false;
    }

    /**
     * Checks if a mailbox exists.
     * @param host the host for the mailbox
     * @param mailbox the mailbox
     * @return true if the mailbox (and its host of course) exists
     */
    private static boolean exists(String host, String mailbox) {
        try {
            return lexicom.hasProperty(ILexiCom.MAILBOX, new String[] {host, mailbox}, "alias");
        } catch (Exception ignore) {}
        return false;
    }

    /**
     * Creates a new user mailbox.
     * @param host which host should contain the mailbox
     * @param user the mailbox alias (user name)
     * @param password the password to set
     */
    private static void createUser(String host, String user, String password) {
        try {
            report("adding user "+user+" with password "+password);
            if (!exists(host, user)) {
                String[] path = new String[] {host, user};
                if (exists(host, templateOption)) {
                    lexicom.clone(ILexiCom.MAILBOX, new String[] {host, templateOption}, user, false);
                } else {
                    report("cannot find mailbox template "+templateOption+": creating a new one");
                    lexicom.create(ILexiCom.MAILBOX, path, false);
                }
                lexicom.setProperty(ILexiCom.MAILBOX, path, "Password", password);
                lexicom.save(host);
            } else {
                error("mailbox for user "+user+" already exists");
            }
        } catch (Exception e) {
            error("error creating user "+user);
            e.printStackTrace(System.err);
        }
    }
    
    /*------------------------------------------------------------------------*
     * Main program                                                           *
     *------------------------------------------------------------------------*/

    public static void main(String[] args) {
        String[] users = parseArgs(args);
        if (users!=null && users.length>0) {
            try {
                lexicom = LexiComFactory.getVersaLex(LexiComFactory.HARMONY,
                                                     homeOption.getAbsolutePath(),
                                                     LexiComFactory.CLIENT_ONLY);
                String host = resolveHost(protocolOption);
                if (host!=null) {
                    for (String user : users) {
                        String[] userpass = user.split(":", 2);
                        if (userpass.length==2) {
                            createUser(host, userpass[0], userpass[1]);
                        } else {
                            createUser(host, user, "password");
                        }
                    }
                }
            } catch (Exception e) {
                error("an error occurred");
                e.printStackTrace(System.err);
            } finally {
                if (lexicom!=null) {
                    try {
                        lexicom.close();
                    } catch (Exception ignore) {}
                    System.exit(0);
                }
            }
        }

    }
}
