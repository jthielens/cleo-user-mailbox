# README #

This example shows the use of the Cleo Harmony ILexiCom API in CLIENT_ONLY mode, where a Java application connects to a running Harmony instance over RMI to manipulate its configuration, in this case for the purposes of creating local FTP, HTTP, or SFTP users.

### How do I get set up? ###

This repository includes an Eclipse (Luna) project that can be used to build the code.  You will need also:
* an installed copy of Cleo Harmony
* a copy of the `LexiCom.jar` file copied from the `lib` directory of your Harmon installation

Note that currently shipping versions of Cleo Harmony use Java 6.

The sample project includes an ANT builder with a JAR task that creates `Main-Class` and `Class-Path` entries to allow invocation as follows:

```
jre/bin/java -jar createuser.jar options user:password ...
```

where the supported options include

```
usage: CreateUserMailbox [options] user ...
options: -d  directory  Harmony home directory (defaults to .)
         -h  name         local user host to use
         -p  protocol     protocol: ftp (default), sftp, or http
         -t  name         template mailbox to use (defaults to myTradingPartner)
```

You should copy the `createuser.jar` to the Harmony installation directory for invocation.  I worked out the `Class-Path` entries, relative to `.` assuming the Harmony installation directory, as a minimal subset for the example by trial-and-error.  If you extend the sample you may have to make additions.